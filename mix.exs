defmodule MBU.Mixfile do
  use Mix.Project

  @version "3.0.0"

  def project do
    [
      app: :mbu,
      version: @version,
      elixir: "~> 1.6",
      name: "MBU: Mix Build Utilities",
      docs: [
        main: "readme",
        extras: ["README.md", "CHANGELOG.md"],
        source_ref: @version,
        source_url: "https://gitlab.com/Nicd/mbu"
      ],
      description: description(),
      package: package(),
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    # Specify extra applications you'll use from Erlang/Elixir
    [extra_applications: [:logger]]
  end

  defp package do
    [
      name: :mbu,
      maintainers: ["Mikko Ahlroth <mikko.ahlroth@gmail.com>"],
      licenses: ["BSD 3-clause"],
      links: %{
        "GitLab" => "https://gitlab.com/Nicd/mbu"
      }
    ]
  end

  defp description do
    """
    MBU is a collection of build utilities for Mix to make it easier to build your project,
    for example building the front end. It supports task dependencies and watching
    directories.
    """
  end

  # Dependencies can be Hex packages:
  #
  #   {:my_dep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:my_dep, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:ex_doc, "~> 0.18.3", only: :dev},
      {:file_system, "~> 0.2.4"}
    ]
  end
end
