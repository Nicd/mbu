# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

# This configuration is loaded before any dependency and is restricted
# to this project. If another project depends on this project, this
# file won't be loaded nor affect the parent project. For this reason,
# if you want to provide default values for your application for
# 3rd-party users, it should be done in your "mix.exs" file.

# You can configure for your application as:
#
#     config :mbu, key: :value
#
# And access this configuration in your application as:
#
#     Application.get_env(:mbu, :key)
#
# Or configure a 3rd-party app:
#
#     config :logger, level: :info
#

# It is also possible to import configuration files, relative to this
# directory. For example, you can emulate configuration per environment
# by uncommenting the line below and defining dev.exs, test.exs and such.
# Configuration from the imported file will override the ones defined
# here (which is why it is important to import them last).
#
#     import_config "#{Mix.env}.exs"

config :mbu,
  # Should tasks generate an `out_path/0` function by default with an autogenerated path? If
  # autogeneration is used in any task, the `:tmp_dir` setting must be set. Can be overridden per
  # task by setting `:auto_path`.
  auto_paths: false,

  # Path to the directory where temporary task artifacts will be stored by tasks.
  tmp_path: nil,

  # Should output paths be automatically created when task starts? If `true`, the path specified
  # by `out_path/0` will be created when task is run. Can be overridden per task by setting
  # `:create_out_path`.
  create_out_paths: false
