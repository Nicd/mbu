defmodule MBUTest do
  use ExUnit.Case

  @tmp_path Path.expand("_tmp")

  setup do
    Application.put_env(:mbu, :auto_paths, false)
    Application.put_env(:mbu, :create_out_paths, false)
    Application.put_env(:mbu, :tmp_path, nil)

    on_exit(fn -> File.rm_rf(@tmp_path) end)
  end

  # Dependency functions to call in tasks
  defmodule Depfuns do
    def foo(), do: send(Process.whereis(:deps_test_runner), :foo)
    def bar(), do: send(Process.whereis(:deps_test_runner), :bar)
    def foo_no(), do: send(Process.whereis(:no_deps_test_runner), :foo)
    def bar_no(), do: send(Process.whereis(:no_deps_test_runner), :bar)
  end

  test "that a module using the task macro has a run function that takes one argument" do
    defmodule Mix.Task.Foo1 do
      use MBU.BuildTask

      task args do
        assert args == [1]
      end
    end

    assert Mix.Task.Foo1.run([1]) == :ok
  end

  test "that the macro works when given _ to denote ignored args" do
    defmodule Mix.Task.Foo2 do
      use MBU.BuildTask

      task _ do
        :ok
      end
    end

    assert Mix.Task.Foo2.run([]) == :ok
  end

  test "that deps are run before the task" do
    defmodule Mix.Task.Foo3 do
      use MBU.BuildTask

      @deps [
        &Depfuns.foo/0,
        &Depfuns.bar/0
      ]

      task _ do
        :ok
      end
    end

    Process.register(self(), :deps_test_runner)

    Mix.Task.Foo3.run([])
    assert_receive :foo
    assert_receive :bar
  end

  test "that deps are not run if deps: false is given" do
    defmodule Mix.Task.Foo4 do
      use MBU.BuildTask

      @deps [
        &Depfuns.foo_no/0,
        &Depfuns.bar_no/0
      ]

      task _ do
        :ok
      end
    end

    Process.register(self(), :no_deps_test_runner)

    Mix.Task.Foo4.run([1, 2, 3, 4, 5, deps: false])
    refute_receive :foo
    refute_receive :bar
  end

  test "that automatic directory is created for task when auto paths are used" do
    auto_path_config()

    defmodule Mix.Task.Foo5 do
      use MBU.BuildTask

      task _ do
        :ok
      end
    end

    Mix.Task.Foo5.run([])
    assert File.exists?(Path.join([@tmp_path, "task.foo5"]))
  end

  test "that automatic directory is NOT created when config is false and nothing is specified in use" do
    defmodule Mix.Task.Foo6 do
      use MBU.BuildTask

      task _ do
        :ok
      end
    end

    Mix.Task.Foo6.run([])
    assert not File.exists?(Path.join([@tmp_path, "task.foo6"]))
  end

  test "that automatic directory is NOT created when specified not to" do
    auto_path_config()

    defmodule Mix.Task.Foo7 do
      use MBU.BuildTask, create_out_path: false

      task _ do
        :ok
      end
    end

    Mix.Task.Foo7.run([])
    assert not File.exists?(Path.join([@tmp_path, "task.foo7"]))
  end

  test "that automatic directory IS created when specified to even if config is false" do
    Application.put_env(:mbu, :tmp_path, @tmp_path)

    defmodule Mix.Task.Foo8 do
      use MBU.BuildTask, auto_path: true, create_out_path: true

      task _ do
        :ok
      end
    end

    Mix.Task.Foo8.run([])
    assert File.exists?(Path.join([@tmp_path, "task.foo8"]))
  end

  test "that out_path/0 is NOT created by default" do
    defmodule Mix.Task.Foo9 do
      use MBU.BuildTask

      task _ do
        :ok
      end
    end

    assert not Kernel.function_exported?(Mix.Task.Foo9, :out_path, 0)
  end

  test "that out_path/0 IS created if overridden" do
    Application.put_env(:mbu, :tmp_path, @tmp_path)

    defmodule Mix.Task.Foo10 do
      use MBU.BuildTask, auto_path: true

      task _ do
        :ok
      end
    end

    assert Kernel.function_exported?(Mix.Task.Foo10, :out_path, 0)
  end

  defp auto_path_config() do
    Application.put_env(:mbu, :auto_paths, true)
    Application.put_env(:mbu, :create_out_paths, true)
    Application.put_env(:mbu, :tmp_path, @tmp_path)
  end
end
